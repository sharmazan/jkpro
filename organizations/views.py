from django.shortcuts import render, get_object_or_404

from organizations.models import Organization

# Create your views here.

def organizations_list(request):
    organizations = Organization.objects.all()
    return render(request, 'organizations/organizations_list.html', {'organizations': organizations, })

def organization_detail(request, organization_id):
    organization = get_object_or_404(Organization, pk=organization_id)
    return render(request, 'organizations/organization_detail.html', {'organization': organization})

def add_organization(request):
	return 