from django.conf.urls import patterns, url

from organizations import views

urlpatterns = patterns('',
    url(r'^$', views.organizations_list, name='organizations_list'),
    url(r'^(?P<organization_id>\d+)/$', views.organization_detail, name='organization_detail'),
    url(r'^add/$', views.add_organization, name='add_organization'),
)