# -*- coding: utf-8 -*-
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from taggit.managers import TaggableManager

# Create your models here.



class Organization(MPTTModel):
    title = models.CharField(max_length=150)
    description = models.TextField(blank=True)
    structure = models.TextField(blank=True)
    staff = models.TextField(blank=True)
    contacts = models.TextField(blank=True)
    website = models.URLField(blank=True)
    karma = models.IntegerField(default = 0)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    class MPTTMeta:
        order_insertion_by = ['title']    
    def __unicode__(self):
        return self.title

