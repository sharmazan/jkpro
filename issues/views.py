from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.template import Context, loader
from django import forms

from issues.models import Person, Problem, Solution

# Create your views here.

def homepage(request):
    return render(request, 'base.html')

def problems_list(request):
    problems = Problem.objects.all()
    return render(request, 'issues/problems_list.html', {'problems': problems, })

def problem_detail(request, problem_id):
    problem = get_object_or_404(Problem, pk=problem_id)
    return render(request, 'issues/problem_detail.html', {'problem': problem})

class ProblemForm(forms.ModelForm):
    class Meta:
        model = Problem

def add_problem(request, problem_id=None):
    if problem_id is not None:
        problem = get_object_or_404(Problem, id=problem_id)
    else:
        problem = None

    if request.GET:
        form = ProblemForm(request.GET, instance=problem)
        if form.is_valid():
            problem = form.save()
            return redirect(reverse('problem_detail', args=[problem.id]))
    else:
        form = ProblemForm(instance=problem)
        
    return render(request, "issues/problem_detail.html",
                    {'problem': problem, 'form': form })


def person_profile(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    return render(request, 'issues/profile_detail.html', {'person': person})

def persons_list(request):
    users_list = Person.objects.all()
    return render(request, 'issues/profiles_list.html', {'users_list': users_list, })