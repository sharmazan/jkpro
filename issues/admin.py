from django.contrib import admin
from models import Problem, Solution, Person, Address, Problem_type

# Register your models here.
admin.site.register(Person)
admin.site.register(Problem)
admin.site.register(Problem_type)
admin.site.register(Solution)
admin.site.register(Address)