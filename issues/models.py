# -*- coding: utf-8 -*-
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from taggit.managers import TaggableManager

# Create your models here.



class Person(models.Model):
    username = models.CharField(max_length=50)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50, blank=True)
    birthday = models.DateField(blank=True, null=True)
    invited_by = models.ForeignKey('self', blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True)
    email = models.CharField(max_length=50, blank=True)
    website = models.URLField(blank=True)
    facebook = models.URLField(blank=True)
    karma = models.IntegerField(default = 0)
    def __unicode__(self):
        return self.firstname+" "+self.lastname+" ("+self.username+")"


class Address(models.Model):
    index = models.CharField(max_length = 5, blank = True, null = True)
    town = models.CharField(max_length = 50)
    street = models.CharField(max_length = 50)
    house = models.CharField(max_length = 10)
    flat = models.CharField(max_length = 10, blank = True, null = True) 
    def __unicode__(self):
        return self.town+", "+self.street+", "+self.house+", "+self.flat


class Problem_type(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    def __unicode__(self):
        return self.title


class Problem(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    author = models.ForeignKey(Person)
    rating = models.IntegerField(default = 0)
    asked = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    address = models.ForeignKey(Address)
    category = models.ForeignKey(Problem_type)
    # tags = TaggableManager()

    STREET = 'S'
    HOUSE = 'H'
    PORCH = 'P'
    FLAT = 'F'
    PLACE_CHOICES = (
        (STREET, 'Улица'),
        (HOUSE, 'Дом'),
        (PORCH, 'Подъезд'),
        (FLAT, 'Квартира'),
    )
    place = models.CharField(max_length=1, choices=PLACE_CHOICES)

    OPEN = 'O'
    CANCELLED = 'C'
    SOLVED = 'S'
    VERIFIED = 'V'
    
    STATUS_CHOICES = (
        (OPEN, 'Открыта'),
        (CANCELLED, 'Отменена'),
        (SOLVED, 'Решена'),
        (VERIFIED, 'Проверена'),
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)

    def __unicode__(self):
        return self.title


class Solution(models.Model):
    problem = models.ForeignKey(Problem)
    description = models.TextField(blank=True)
    autor = models.ForeignKey(Person)
    rating = models.IntegerField()
    proposed = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)


# class Region(MPTTModel):
#     title = models.CharField(max_length=50)
#     description = models.TextField()
#     parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
#     region_type = models.ForeignKey(Region_type)
#     def __unicode__(self):
#         return sefl.title

#     class MPTTMeta:
#         order_insertion_by = ['title']


# class Region_type(models.Model):
#     title = models.CharField(max_length=50)
#     description = models.TextField()
#     def __unicode__(self):
#         return sefl.title


# class Object(models.Model):
#     region = models.ForeignKey(Region)
#     house = models.CharField(max_length = 10)
#     flat = models.CharField(max_length = 10, blank = True, null = True)
#     owner = models.TextField()

#     def __unicode__(self):
#         return sefl.title

