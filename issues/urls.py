from django.conf.urls import patterns, url

from issues import views

urlpatterns = patterns('',
    url(r'^$', views.problems_list, name='problems_list'),
    # ex: /issues/5/
    url(r'^(?P<problem_id>\d+)/$', views.problem_detail, name='problem_detail'),
    url(r'^add/$', views.add_problem, name='add_problem'),


)