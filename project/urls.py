from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from issues import views as issues_views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', 'issues.views.homepage', name='homepage'),
    url(r'^issues/', include('issues.urls')),
    url(r'^organizations/', include('organizations.urls')),

    url(r'^profiles/(?P<person_id>\d+)/$', issues_views.person_profile, name='person_profile'),
    url(r'^profiles/$', issues_views.persons_list, name='persons_list'),

    url(r'^admin/', include(admin.site.urls)),
)
